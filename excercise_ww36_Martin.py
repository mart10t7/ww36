import RPi.GPIO as GPIO # Importing gpio library
import time # Importing time library
GPIO.setmode(GPIO.BCM) # Setting the pins mode to Broadcom SOC Channel
GPIO.setwarnings(False) # Turning off warnings
GPIO.setup(18,GPIO.OUT) # Sets voltage to 18th GPIO pin
GPIO.output(18,GPIO.HIGH)
time.sleep(1) # LED remaining on for 1 second
GPIO.output(18,GPIO.LOW) # Turning off GPIO 18 pin

GPIO.setup(23,GPIO.OUT)
GPIO.output(23,GPIO.HIGH)
time.sleep(1)
GPIO.output(23,GPIO.LOW)

GPIO.setup(24,GPIO.OUT)
GPIO.output(24,GPIO.HIGH)
time.sleep(1)
GPIO.output(24,GPIO.LOW)